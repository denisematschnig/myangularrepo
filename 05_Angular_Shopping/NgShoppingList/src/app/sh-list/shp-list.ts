import {Item } from "./sh_item";
import { forEach } from '@angular/router/src/utils/collection';
import { of } from 'rxjs';

export class ShoppingList{
   
    public items: Array<Item>;
    private wholeList : number;
    public sum : number;

    constructor(){
        this.items = new Array();
        this.wholeList = 0;
        // this.renderItem(this.items);
    }

    public addItem(i: string, p: number,   q: number){
        var newItem;
        if(i === "Enter name here"){
            alert("Please enter a name");
            return;
        }
        
        newItem = new Item(i, p, q);
        this.items.push(newItem);
        // this.renderItem(this.items);
    }

    public removeItem(its : Array<Item>){

        var position;

        for( var iy of its){
            for(var e of this.items){
                if(e === iy){
                    position = this.items.indexOf(e);
                    this.items.splice(position, 1);
                }
            }
        }    
    }

    public printResult(){
        for(var it of this.items){
            console.log(it.Item + "\t" + it.Price + "\t" + it.Quant + "\t" + it.Total.toFixed(2));
        }
    }

    public getTotalPrice() : number{
       this.sum = 0;
       this.wholeList = 0;
        for(var i of this.items){
            this.wholeList += i.Total; 
            
        }
        this.sum = this.wholeList;
        return this.wholeList;
    }

    public renderItem(its : Array<Item>){

        for(var bob of its){
            var itemNode = "<tr><td>" + bob.Item +"</td><td>" + bob.Price +"</td><td>" + bob.Quant +"</td><td>" + bob.Total.toFixed(2) +"</td></tr>";

            var p =  document.getElementById('items-table');
            p.innerHTML += itemNode;
           
        }
    }
}
