export class Item{
    private item : string;
    private price : number;
    private quant : number;
    private total : number;
    private _isSelected : boolean;
    constructor(item: string, price: number, quant: number){
        this.item = item;
        this.price = price;
        this.quant = quant;
        this.total = (this.price * this.quant);  
        this.isSelected = false;
          
    }

    
    public get isSelected() : boolean {
        return this._isSelected;
    }
    public set isSelected(v : boolean) {
        this._isSelected = v;
    }
    

    //Item
    public get Item() : string {
        return this.item;
    }
    public set Item(v : string) {
        this.item = v;
    }

    //Price
    public get Price() : number {
        return this.price;
    }
    public set Price(v : number) {
        this.price = v;
    }

    //Quantity
    public get Quant() : number {
        return this.quant;
    }
    public set Quant(v : number) {
        this.quant = v;
    }

    //Quantity
    public get Total() : number {
        return this.total;
    }
    public set Total(v : number) {
        this.total = v;
    }
    
   
}