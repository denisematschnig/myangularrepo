import { Component, OnInit } from '@angular/core';
import { ShoppingList } from "./shp-list";
import { Item } from "./sh_item";
import { forEach } from '@angular/router/src/utils/collection';



@Component({
  selector: 'app-sh-list',
  templateUrl: './sh-list.component.html',
  styleUrls: ['./sh-list.component.scss']
})
export class ShListComponent implements OnInit {

  public newItem : Item;
  public newItemDef : string = "Enter name here";
  public newPrice : number = 0;
  public newTotal : number = 0;
  public sh : ShoppingList;
  public fullCost : number = 0;
  public stringCost : string;
  public toDelete : Array<Item>;
  public selectedItem : Item;
  public defaultItem : Item;


  constructor() 
  {
    this.sh = new ShoppingList();
    this.fillList();
    this.stringCost = this.fullCost.toFixed(2);
    this.toDelete = new Array<Item>();
    this.defaultItem = new Item(" ", 0, 0);
    if(this.selectedItem === undefined){
      this.selectedItem = this.defaultItem;
    }
    
  }

  ngOnInit() {
    // for(var e of this.single_items){

    // }
  }

  public fillList()
  {
    this.sh.addItem("Milch", 1.21, 3);
    this.sh.addItem("Eier", 0.38, 9);
    this.sh.addItem("Butter", 1.42, 4);
    this.sh.addItem("Mehl", 2.38, 2);

    
    this.fullCost = this.sh.getTotalPrice();
  }

  public AddItemToList()
  {
    // this.newItem = new Item(this.newItemDef, this.newPrice, this.newTotal);
    this.sh.addItem(this.newItemDef, this.newPrice, this.newTotal);
    // this.fullCost = 0;
    // this.fullCost = this.sh.getTotalPrice();
  }

  public GetTotalCost () : number
  {
   var tots : number = 0;
    for(var i of this.sh.items)
    {
      tots+= i.Total;
    }

    return tots;
  }

  public FormTotalCost() : string {
    var result = this.GetTotalCost().toFixed(2);
    return result;
  }
  

  public selectItem(i : Item) : void{
      console.log(i.isSelected);  
      this.selectedItem = i;
      console.log("What", this.selectedItem);
      i.isSelected =! i.isSelected;
      console.log(i.isSelected);
      if(this.toDelete.indexOf(i) === -1){
        this.toDelete.push(i);
      }
      else{
        this.toDelete.splice(this.toDelete.indexOf(i), 1);
      }


      console.log(this.toDelete);
  }


  public RemoveItemFromList() : void 
  { 
    this.sh.removeItem(this.toDelete);
    
    while(this.toDelete.length != 0){
      this.toDelete.pop();
    } 

    this.selectedItem = this.defaultItem;

  }


  
}
